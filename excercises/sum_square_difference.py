#!/usr/bin/python

def main(number):
    store = []
    store2 = []
    for value in range(0,number + 1):
        sqrs = value * value        
        store.append(sqrs)
        store2.append(value)

    sqrs_sum = sum(store)
    sum_of_sqrs = sum(store2) * sum(store2)

    print sum_of_sqrs - sqrs_sum


if __name__ == "__main__":
    main(100)
