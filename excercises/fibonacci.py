
import sys
import os

store_stuff = []
		
def recur_fibo(n):
    """Recursive function to
    print Fibonacci sequence"""
    if n <= 1:
        return n
    else:
       return(recur_fibo(n-1) + recur_fibo(n-2))
				

if __name__ == "__main__":
	for i in range(34):
		if recur_fibo(i) % 2 is 0:
			store_stuff.append(recur_fibo(i))
		print(sum(store_stuff))