#!/usr/bin/python

import sys

# Probably not the most efficient implementation, but it does work.

def main(a):
    temp = 1
    first = 3

    while True:
        out, number = checkNum(first)
        # if the number is equal to 0 on 2 occasions then it is a prime
        if out is 2:
            temp += 1
            if temp == a:
                print number
                sys.exit(1)
        # started on 3, iter by 2 to ensure odd number.
        first += 2

def checkNum(number):
    b = 1
    inc = 0
    # checks every number to see if its modulor 
    while b <= number:
        if number % b is 0:
            inc += 1
        b += 1        

    return inc, number
    


if __name__ == "__main__":
    # Generates the 10001st prime number
    main(10001)
