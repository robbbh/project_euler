#!/usr/bin/python

import sys

def main(number):
    
    iter_num = number

    while True:
        temp = 0
        for value in range(1,number + 1):
            # increase iter_num by 1 if it's not evenly divisible
            if iter_num % value is not 0:
                iter_num += 1
                break

            else:
            # increment by one when hit an evenly divisible number
               temp += 1
        
        # if temp hits the input number, the answer is given
        if temp is number:
            print iter_num
            sys.exit(1)


if __name__ == "__main__":

    # smallest positive number that is evenly divisible 
    # by all of the numbers from 1 to 20
    main(20)
